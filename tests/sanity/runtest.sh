#!/usr/bin/bash
set -x
CIFS_SHARE=$PWD/cifs-share
CIFS_MNT=$PWD/cifs-mnt
mount.cifs -V
cp /etc/samba/smb.conf /etc/samba/smb.conf.bark
cat << __EOF__ >/etc/samba/smb.conf
[cifs]
path=$CIFS_SHARE
writeable=yes
__EOF__

testparm -s
mkdir -p $CIFS_SHARE $CIFS_MNT
chcon -t samba_share_t $CIFS_SHARE
setsebool -P samba_export_all_rw on
echo -e "redhat\nredhat" | smbpasswd -a root -s
systemctl start smb
sleep 5
mount //localhost/cifs $CIFS_MNT -o user=root,password=redhat
ls -l $CIFS_MNT
umount $CIFS_MNT
cp /etc/samba/smb.conf.bark /etc/samba/smb.conf
rm -rf $CIFS_SHARE $CIFS_MNT
